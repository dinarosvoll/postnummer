import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
   if (event.target.value !instanceof String) {
    this.setState({value: event.target.value});
  }}
  
  render() {
    return (
      <div className="App">
        <p>Fyll inn postnummer:</p>
        <input type="number" value={this.state.value} maxLength="4" onChange={this.handleChange}/>
        <p>Poststed:</p>
        <input type="text" defaultValue={this.state.value}/>
      </div>
    );
  }
}

export default App;
